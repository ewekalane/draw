//
//  SignupViewController.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/20/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit
import FirebaseAuth

protocol userSignUpDelegate {
    func userDidSignUp()
}

class SignUpViewController: BaseViewController {
    
    var delegate: userSignUpDelegate?
    
    lazy var emailAddressTextField: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.placeholder = "email"
        textfield.autocorrectionType = .no
        textfield.autocapitalizationType = .none
        textfield.textAlignment = .center
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var passwordTextField: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.placeholder = "password"
        textfield.textAlignment = .center
        textfield.isSecureTextEntry = true
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var passwordRepeatTextField: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.placeholder = "Re-enter password"
        textfield.textAlignment = .center
        textfield.isSecureTextEntry = true
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign up", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    lazy var vStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Create an Account"
        setupView()
    }
    
}

extension SignUpViewController {
    
    private func setupView() {
        
        signUpButton.addTarget(self, action: #selector(createUser), for: .touchUpInside)
        
        view.addSubview(vStack)
        vStack.addArrangedSubview(emailAddressTextField)
        vStack.addArrangedSubview(passwordTextField)
        vStack.addArrangedSubview(passwordRepeatTextField)
        vStack.addArrangedSubview(signUpButton)
        
        NSLayoutConstraint.activate([
            vStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            vStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            vStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            vStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15)
        ])
    }
}


extension SignUpViewController {
    
    @objc func createUser(_ sender: Any) {
        
        guard
            emailAddressTextField.isvalidEmail() == true,
            passwordTextField.isvalidPassword() == true,
            let emailAddress = emailAddressTextField.text,
            let password = passwordTextField.text
        else {
            showAlert(message: "Passwords must contain uppercase letter, alphanumeric and minimum 8 characters")
            return
        }
        
        guard passwordRepeatTextField.text == passwordTextField.text else {
            showAlert(message: "Passwords does not match")
            return
        }
        
        Auth.auth().createUser(withEmail: emailAddress, password: password, completion: { (result, error) in
            if error != nil  {
                self.showAlert(message: error?.localizedDescription)
                return
            }
          
            self.delegate?.userDidSignUp()
        })
    }
}
