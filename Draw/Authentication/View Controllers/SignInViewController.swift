//
//  SignInViewController.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/19/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignInViewController: BaseViewController {
    lazy var usernameTextfield: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.placeholder = "email"
        textfield.textAlignment = .center
        textfield.autocorrectionType = .no
        textfield.autocapitalizationType = .none
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var passwordTextfield: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.isSecureTextEntry = true
        textfield.placeholder = "password"
        textfield.textAlignment = .center
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var signInButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign In", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textAlignment = .center
        return button
    }()
    
    lazy var vStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var hStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Sign In"
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (Auth.auth().currentUser != nil) {
            signIn()
        }
    }
}


extension SignInViewController {
    private func setupView() {
        signInButton.addTarget(self, action: #selector(logIn), for: .touchUpInside)
        signUpButton.addTarget(self, action: #selector(signUp), for: .touchUpInside)
        
        view.addSubview(vStack)
        vStack.addArrangedSubview(usernameTextfield)
        vStack.addArrangedSubview(passwordTextfield)
        vStack.addArrangedSubview(hStack)
        hStack.addArrangedSubview(signInButton)
        hStack.addArrangedSubview(signUpButton)
        
        NSLayoutConstraint.activate([
            vStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            vStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            vStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            vStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15)
        ])
    }
}

extension SignInViewController: userSignUpDelegate {
    func userDidSignUp() {
        dismiss(animated: true, completion: {
            self.signIn()
        })
    }
    
    private func signIn() {
        usernameTextfield.text = nil
        passwordTextfield.text = nil
        let mainViewController = MainViewController()
        self.navigationController?.pushViewController(mainViewController, animated: false)
    }
}

extension SignInViewController {
    @objc private func signUp() {
        let signupViewController = SignUpViewController()
        signupViewController.delegate = self
        let navController = UINavigationController(rootViewController: signupViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc private func logIn() {
        guard usernameTextfield.text?.count != 0,
              passwordTextfield.text?.count != 0,
              let username = usernameTextfield.text,
              let password = passwordTextfield.text else {
                return
        }
        
        Auth.auth().signIn(withEmail: username, password: password) { user, error in
            if error != nil {
                self.showAlert(message: error?.localizedDescription)
                return
            }
            self.signIn()
        }
    }
}

