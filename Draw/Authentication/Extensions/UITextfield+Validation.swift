//
//  UITextfield+Validation.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/20/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//
import UIKit

extension UITextField {
    func isvalidPassword() -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}")
        return passwordTest.evaluate(with: self.text)
    }
    
    func isvalidEmail() -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self.text)
    }
}
