//
//  DrawBaseViewController.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/23/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class BaseAuthViewController: BaseViewController {
    
    var db: Firestore!
    var currentUser: User!

    override func viewDidLoad() {
        super.viewDidLoad()

        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        
        guard let u = Auth.auth().currentUser else {
            self.navigationController?.popToRootViewController(animated: true)
            return
        }
        currentUser = u
    }
}
