//
//  BaseViewController.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/24/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var messageAlert: UIAlertController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showAlert(message: String?) {
        messageAlert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        messageAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(messageAlert, animated: true)
    }
}
