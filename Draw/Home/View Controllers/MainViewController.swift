//
//  MainViewController.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/19/20.
//  Copyright © 2020 Talkspace. All rights reserved.

import UIKit
import FirebaseAuth
import FirebaseFirestore

class MainViewController: BaseAuthViewController {
    
    let tableView = UITableView()
    
    var draws = [DrawVM]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Drawings"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New", style: .plain, target: self, action: #selector(createNewDrawing))
        
        tableView.register(DrawCell.self, forCellReuseIdentifier: DrawCell.identifier)
        setupview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCollection()
    }
    
    private func setupview() {
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        ])
    }
    
}

extension MainViewController {
    @objc func logout() {
        do {
            try Auth.auth().signOut()
            self.navigationController?.popToRootViewController(animated: true)
        } catch {
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @objc func createNewDrawing() {
        let drawingViewController = DrawingViewController()
        self.navigationController?.pushViewController(drawingViewController, animated: true)
    }
    
    private func getCollection() {
        db.collection("draws").getDocuments() { (querySnapshot, err) in
            if let err = err {
                self.showAlert(message: err.localizedDescription)
                return
            } else {
                
                guard let querySnapshot = querySnapshot else {
                    self.showAlert(message: "Error occurred")
                    return
                }
                
                var documents = [DrawVM]()
                for document in querySnapshot.documents {
                    let vm = DrawVM(document: document)
                    documents.append(vm)
                }
                
                self.draws = documents
            }
        }
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return draws.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DrawCell.identifier, for: indexPath) as! DrawCell
        cell.draw = draws[indexPath.row]
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let draw = draws[indexPath.row]
        let drawingViewController = DrawingViewController()
        drawingViewController.viewModel = draw
        self.navigationController?.pushViewController(drawingViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteItem(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return draws[indexPath.row].id == currentUser.uid
    }
    
    private func deleteItem(indexPath: IndexPath) {
        let draw = draws[indexPath.row]
        tableView.beginUpdates()
        draw.document.reference.delete()
        draws.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
}

