//
//  DrawCellVM.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/23/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

struct DrawVM {
    let document: QueryDocumentSnapshot

    init(document: QueryDocumentSnapshot) {
        self.document = document
    }
}

extension DrawVM {
    var image: UIImage? {
        guard let data = document.data()["image_blob"] as? Data else {
            return nil
        }
        return UIImage(data: data)
    }
    
    var id: String? {
        return document.data()["user_id"] as? String
    }
    
    var date: String? {
        return document.data()["created_at"] as? String
    }
    
    var info: String? {
        return "\(id ?? "")\n\n\(date ?? "")"
    }
}
