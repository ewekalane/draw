//
//  SavedDrawCell.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/19/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit

class DrawCell: UITableViewCell {
    
    static let identifier = "DrawCellId"
    
    var draw: DrawVM? {
        didSet {
            guard let draw = draw else { return }
            previewImageView.image =  draw.image
            titleLabel.text = draw.info
        }
    }
    
    let previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = .white
        contentView.addSubview(previewImageView)
        contentView.addSubview(titleLabel)

        NSLayoutConstraint.activate([
            previewImageView.widthAnchor.constraint(equalToConstant: 80),
            previewImageView.heightAnchor.constraint(equalToConstant: 80),
            previewImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            previewImageView.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            previewImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
