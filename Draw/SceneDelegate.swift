//
//  SceneDelegate.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/19/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit
import FirebaseAuth

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(windowScene: windowScene)
        let navController = UINavigationController()
        let mainController = SignInViewController()
        navController.viewControllers = [mainController]
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}

