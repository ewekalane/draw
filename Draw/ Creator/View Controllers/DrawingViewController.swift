//
//  DrawingViewController.swift
//  Draw
//
//  Created by Eseosa Eriamiatoe on 8/23/20.
//  Copyright © 2020 Talkspace. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class DrawingViewController: BaseAuthViewController {
    
    var documentSnapshot: DocumentSnapshot? = nil
    var documentReference: DocumentReference? = nil
    
    var viewModel: DrawVM? {
        didSet {
            guard let vm = viewModel else { return }
            documentSnapshot = vm.document
            documentReference = documentSnapshot?.reference
        }
    }
    
    var lastPoint = CGPoint.zero
    var color = UIColor.black
    var brushWidth: CGFloat = 10.0
    var swiped = false
    
    lazy var tempImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var mainImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var brushStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.backgroundColor = .black
        stack.isHidden = true
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupView()
        
        tempImageView.image = viewModel?.image
        view.isUserInteractionEnabled = false
        
        if viewModel?.id == currentUser.uid || viewModel?.image == nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(save))
            brushStack.isHidden = false
            view.isUserInteractionEnabled = true
        }
    }
}


extension DrawingViewController {
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(view.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        tempImageView.image?.draw(in: view.bounds)
        
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        
        context.strokePath()
        
        tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        swiped = false
        lastPoint = touch.location(in: view)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        swiped = true
        let currentPoint = touch.location(in: view)
        drawLine(from: lastPoint, to: currentPoint)
        
        lastPoint = currentPoint
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            // draw a single point
            drawLine(from: lastPoint, to: lastPoint)
        }
        // Merge tempImageView into mainImageView
        UIGraphicsBeginImageContext(mainImageView.frame.size)
        mainImageView.image?.draw(in: view.bounds, blendMode: .normal, alpha: 1.0)
        tempImageView.image?.draw(in: view.bounds, blendMode: .normal, alpha: 1.0)
        mainImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        tempImageView.image = nil
    }
    
    @objc func brushPressed(_ sender: UIButton) {
        guard let brush = Brush(tag: sender.tag) else {
            return
        }
        color = brush.color
    }
}


extension DrawingViewController {
    private func setupView() {
        view.addSubview(mainImageView)
        view.addSubview(tempImageView)
        view.addSubview(brushStack)
        
        NSLayoutConstraint.activate([
            mainImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainImageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            
            tempImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tempImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tempImageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tempImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            
            brushStack.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            brushStack.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            brushStack.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            brushStack.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        //Add brush options
        for i in 1...11 {
            let button:UIButton = UIButton()
            let brush = Brush.init(tag: i)
            button.backgroundColor = brush?.color
            button.tag = i
            button.addTarget(self, action:#selector(brushPressed), for: .touchUpInside)
            brushStack.addArrangedSubview(button)
        }
    }
}


extension DrawingViewController {
    @objc func save(_ sender: UIButton) {
        guard let data = mainImageView.image?.pngData() else {
            return
        }
        
        if documentReference == nil {
            documentReference = db.collection("draws").addDocument(data: [
                "user_id": currentUser.uid,
                "user_email": currentUser.email ?? "",
                "created_at": Date().description(with: .current),
                "image_blob": data
            ]) { error in
                if let err = error {
                    self.showAlert(message: err.localizedDescription)
                    return
                }
                self.showAlert(message: "Saved")
            }
        } else {
            documentReference?.updateData(["image_blob": data])
            self.showAlert(message: "Updated")
        }
    }
}
